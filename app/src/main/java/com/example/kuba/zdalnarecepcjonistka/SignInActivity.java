package com.example.kuba.zdalnarecepcjonistka;

import android.content.Intent;
import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class SignInActivity extends AppCompatActivity{

    private DatabaseHelper zdalnaRecepcjonistka;
    private EditText editAddressEmail;
    private EditText editPassword;
    private Button btnZaloguj;
    private Button btnZarejestrujSieTeraz;

    public static int idUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);
        zdalnaRecepcjonistka = new DatabaseHelper(this);


        //  zapisanie wartosci pol tekstowych do zmiennych
        editAddressEmail = findViewById(R.id.editTextAdresEmailLogowanie);
        editPassword = findViewById(R.id.editTextHasloLogowanie);
        btnZaloguj = findViewById(R.id.buttonZaloguj);
        btnZarejestrujSieTeraz = findViewById(R.id.buttonZarejestrujSieTeraz);

        Zaloguj();
        Rejestracja();
    }

    /**
     * metoda oblsuguje przycisk Zaloguj
     */
    private void Zaloguj(){
        btnZaloguj.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { trySignIn(); }

            /**
             * metoda hashuje haslo wprowadzone przez uzytkownika i wywoluje funkcje porwnujaca wprowadzone wartosci z wartosciami w tabeli UZYTKOWNICY
             */
            void trySignIn() {
                String adresEmail = editAddressEmail.getText().toString();
                String haslo = editPassword.getText().toString();
                String hashHaslo = "";
                Intent intentUser = new Intent(SignInActivity.this, UserActivity.class);

                try {
                    hashHaslo = HashPassword.hashPassword(haslo);
                }
                catch (Exception exception) {
                    Log.e("Encryption pass error/S", exception.getMessage());
                }

                if(SprawdzPoprawnoscLogowania(adresEmail, hashHaslo))
                    Toast.makeText(SignInActivity.this,"Niepoprawny e-mail lub hasło!", Toast.LENGTH_SHORT).show();
                else {
                    Toast.makeText(SignInActivity.this,"Zalogowano", Toast.LENGTH_SHORT).show();

                    //  pobranie danych (id, imie, nazwisko, nr telefonu) z tabeli UZYTKOWNICY do zmiennych
                    try {
                        Cursor res = zdalnaRecepcjonistka.pobierzIdUzytkownika(adresEmail, hashHaslo);
                        if (res != null && res.moveToFirst())
                            idUser = res.getInt(0);
                        assert res != null;
                        res.close();
                    }
                    catch (Exception exception) {
                        Log.e("Get user's id error", exception.getMessage());
                    }
                    startActivity(intentUser);
                }
            }

            /**
             * metoda porwnuje dane wprowadzone w polach tekstowych z tabela UZYTKOWNICY
             * param email
             * param haslo
             * return
             */
            boolean SprawdzPoprawnoscLogowania(String email, String haslo) {
                Cursor resEmail = zdalnaRecepcjonistka.checkSignInAddressEmail(email);
                Cursor resPassword = zdalnaRecepcjonistka.checkSignInPassword(haslo);

                if(resEmail.getCount() == 1 && resPassword.getCount() > 0)
                    return false;
                else
                    return true;
            }
        });
    }

    /**
     * metoda obsluguje przycisk 'Zarejestruj sie teraz
     */
    private void Rejestracja() {
        btnZarejestrujSieTeraz.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentZarejestrujSieTeraz = new Intent(SignInActivity.this, RegistrationActivity.class);
                startActivity(intentZarejestrujSieTeraz);
            }
        });
    }


}