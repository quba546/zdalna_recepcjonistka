package com.example.kuba.zdalnarecepcjonistka;

import android.annotation.SuppressLint;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.util.UUID;


public class ReceiveKeyActivity extends AppCompatActivity {

    private DatabaseHelper zdalnaRecepcjonistka;
    private EditText editEmail;
    private EditText editKod;
    private Button btnConnect;
    private Handler handler;
    private AlertDialog alert;
    private int nr_pokoju;

    private static final String TAG = "bluetooth1";
    private final int RECIEVE_MESSAGE = 1;		// Status  for Handler
    private BluetoothAdapter btAdapter = null;
    private BluetoothSocket btSocket = null;
    private final StringBuilder stringBuilder = new StringBuilder();

    private ConnectedThread mConnectedThread;

    // SPP UUID service
    private static final UUID MY_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");

    // MAC-address of Bluetooth module (you must edit this line)
    private static final String address = "20:18:08:23:51:64";

    @SuppressLint("HandlerLeak")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_receive_key);

        zdalnaRecepcjonistka = new DatabaseHelper(this);

        editEmail = findViewById(R.id.editTextReceiveEmail);
        editKod = findViewById(R.id.editTextReceiveKod);
        btnConnect = findViewById(R.id.buttonReceiveConnect);
        Button btnOdbierzKlucz = findViewById(R.id.buttonReceiveOdbierzKlcz);

        handler = new Handler() {
            public void handleMessage(android.os.Message msg) {
                switch (msg.what) {
                    case RECIEVE_MESSAGE:													// if receive massage
                        byte[] readBuf = (byte[]) msg.obj;
                        String strIncom = new String(readBuf, 0, msg.arg1);			// create string from bytes array
                        switch (strIncom){
                            case "0":
                                AlertDialog.Builder builder = new AlertDialog.Builder(ReceiveKeyActivity.this);     // wyswietlanie okna dialogowego
                                alert = builder.create();
                                alert.setCancelable(true);
                                alert.setTitle("Informacja");
                                alert.setMessage("Kod poprawny. Otwórz szafkę nr " + nr_pokoju + ", aby wyjąć klucz do pokoju");
                                alert.show();
                                zdalnaRecepcjonistka.dodajPobranieKlucza(editKod.getText().toString());
                                editEmail.getText().clear();
                                editKod.getText().clear();
                                break;
                            case "4":
                                alert.cancel();
                                Toast.makeText(ReceiveKeyActivity.this,"Szafka nr 1 otwarta", Toast.LENGTH_SHORT).show();
                                break;
                            case "5":
                                alert.cancel();
                                Toast.makeText(ReceiveKeyActivity.this,"Szafka nr 2 otwarta", Toast.LENGTH_SHORT).show();
                                break;
                            case "6":
                                alert.cancel();
                                Toast.makeText(ReceiveKeyActivity.this,"Szafka nr 3 otwarta", Toast.LENGTH_SHORT).show();
                                break;
                            case "7":
                                Toast.makeText(ReceiveKeyActivity.this,"Szafka nr 1 zamknięta", Toast.LENGTH_SHORT).show();
                                break;
                            case "8":
                                Toast.makeText(ReceiveKeyActivity.this,"Szafka nr 2 zamknięta", Toast.LENGTH_SHORT).show();
                                break;
                            case "9":
                                Toast.makeText(ReceiveKeyActivity.this,"Szafka nr 3 zamknięta", Toast.LENGTH_SHORT).show();
                                break;
                        }
                        Log.d(TAG, "...String:"+ stringBuilder.toString() +  "Byte:" + msg.arg1 + "...");
                        break;
                }
            }
        };

        btAdapter = BluetoothAdapter.getDefaultAdapter();
        checkBTState();

        btnOdbierzKlucz.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nr_pokoju = zdalnaRecepcjonistka.pobierzIdPokojuDlaKluczaReceive(editEmail.getText().toString(), editKod.getText().toString());

                if(nr_pokoju > 0 && nr_pokoju < 4) {
                    switch (checkBTState()) {               // zabezpieczenie jesli bluetooth zostanie wylaczone po otworzeniu aktwynosci
                        case -1:
                            Toast.makeText(ReceiveKeyActivity.this,"Urządzenie nie obsługuje Bluetooth", Toast.LENGTH_SHORT).show();
                            break;
                        case 0:
                            mConnectedThread.write(Integer.toString(nr_pokoju));
                            break;
                        case 1:
                            Toast.makeText(ReceiveKeyActivity.this,"Włącz Bluetooth, aby pobrać klucz", Toast.LENGTH_SHORT).show();
                            break;
                    }
                }
                else if(nr_pokoju == -1)
                    Toast.makeText(ReceiveKeyActivity.this,"Błędny e-mail lub kod", Toast.LENGTH_SHORT).show();
                else if(nr_pokoju == -2)
                    Toast.makeText(ReceiveKeyActivity.this,"Klucz został już wcześniej pobrany", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private BluetoothSocket createBluetoothSocket(BluetoothDevice device) throws IOException {
        try {
            final Method m = device.getClass().getMethod("createInsecureRfcommSocketToServiceRecord", UUID.class);
            return (BluetoothSocket) m.invoke(device, MY_UUID);
        } catch (Exception e) {
            Log.e(TAG, "Could not create Insecure RFComm Connection",e);
        }
        return  device.createRfcommSocketToServiceRecord(MY_UUID);
    }

    @Override
    public void onResume() {
        super.onResume();

        Log.d(TAG, "...onResume - try connect...");

        BluetoothDevice device = btAdapter.getRemoteDevice(address);    // Set up a pointer to the remote node using it's address.

        // Two things are needed to make a connection:
        //   A MAC address, which we got above.
        //   A Service ID or UUID.  In this case we are using the
        //     UUID for SPP.

        try {
            btSocket = createBluetoothSocket(device);
        } catch (IOException e) {
            errorExit("Fatal Error", "In onResume() and socket create failed: " + e.getMessage() + ".");
        }


        // Discovery is resource intensive.  Make sure it isn't going on
        // when you attempt to connect and pass your message.
        btAdapter.cancelDiscovery();

        // Establish the connection.  This will block until it connects.
        Log.d(TAG, "...Connecting...");
        try {
            btSocket.connect();
            Log.d(TAG, "....Connection ok...");
        } catch (IOException e) {
            try {
                btSocket.close();
            } catch (IOException e2) {
                errorExit("Fatal Error", "In onResume() and unable to close socket during connection failure" + e2.getMessage() + ".");
            }
        }

        // Create a data stream so we can talk to server.
        Log.d(TAG, "...Create Socket...");

        mConnectedThread = new ConnectedThread(btSocket);
        mConnectedThread.start();

        btnConnect.setText("Połączono z: \n" + device.getName());
    }

    @Override
    public void onPause() {
        super.onPause();

        Log.d(TAG, "...In onPause()...");

        try     {
            btSocket.close();
        } catch (IOException e2) {
            errorExit("Fatal Error", "In onPause() and failed to close socket." + e2.getMessage() + ".");
        }
    }

    /**
     * Check for Bluetooth support and then check to make sure it is turned on
     * Emulator doesn't support Bluetooth and will return null
     * metoda zwraca 0, gdy Bluetooth jest wlaczone, 1, gdy jest wylaczone, a -1, gdy urzadzenie nie obsluguje Bluetooth
     * return
     */

    private int checkBTState() {
        if(btAdapter == null) {
            errorExit("Fatal Error", "Bluetooth not support");
            return -1;
        }
        else {
            if (btAdapter.isEnabled()) {
                Log.d(TAG, "...Bluetooth ON...");
                return 0;
            }
            else {
                //Prompt user to turn on Bluetooth
                Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableBtIntent, 1);
                if(!btAdapter.isEnabled()) {
                    Intent intent = new Intent(ReceiveKeyActivity.this, MainActivity.class);
                    startActivity(intent);
                }
                return 1;
            }
        }
    }

    private void errorExit(String title, String message){
        Toast.makeText(getBaseContext(), title + " - " + message, Toast.LENGTH_LONG).show();
        finish();
    }

    private class ConnectedThread extends Thread {
        private final InputStream mmInStream;
        private final OutputStream mmOutStream;

        ConnectedThread(BluetoothSocket socket) {
            InputStream tmpIn = null;
            OutputStream tmpOut = null;

            // Get the input and output streams, using temp objects because
            // member streams are final
            try {
                tmpIn = socket.getInputStream();
                tmpOut = socket.getOutputStream();
            } catch (IOException ignored) { }

            mmInStream = tmpIn;
            mmOutStream = tmpOut;
        }

        public void run() {
            byte[] buffer = new byte[256];  // buffer store for the stream
            int bytes; // bytes returned from read()

            // Keep listening to the InputStream until an exception occurs
            while (true) {
                try {
                    // Read from the InputStream
                    bytes = mmInStream.read(buffer);		// Get number of bytes and message in "buffer"
                    handler.obtainMessage(RECIEVE_MESSAGE, bytes, -1, buffer).sendToTarget();		// Send to message queue Handler
                } catch (IOException e) {
                    break;
                }
            }
        }

        /**
         * Call this from the main activity to send data to the remote device
         * param message
         */
        void write(String message) {
            Log.d(TAG, "...Data to send: " + message + "...");
            byte[] msgBuffer = message.getBytes();
            try {
                mmOutStream.write(msgBuffer);
            } catch (IOException e) {
                Log.d(TAG, "...Error data send: " + e.getMessage() + "...");
            }
        }
    }
}