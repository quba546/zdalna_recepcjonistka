package com.example.kuba.zdalnarecepcjonistka;

import android.content.Intent;
import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


public class RegistrationActivity extends AppCompatActivity {

    private DatabaseHelper zdalnaRecepcjonistka;
    private EditText editAdresEmail;
    private EditText editHaslo;
    private EditText editImie;
    private EditText editNazwisko;
    private EditText editNrTelefonu;
    private Button btnZarejestruj;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        zdalnaRecepcjonistka = new DatabaseHelper(this);

        //  zapisanie do zmiennych wartosci tekstowych pobranych z pol tekstowych
        editAdresEmail = findViewById(R.id.editTextAdresEmail);
        editHaslo = findViewById(R.id.editTextHaslo);
        editImie = findViewById(R.id.editTextImie);
        editNazwisko = findViewById(R.id.editTextNazwisko);
        editNrTelefonu = findViewById(R.id.editTextNrTelefonu);
        btnZarejestruj = findViewById(R.id.buttonZarejestruj);
        Zarejestruj();
    }

    /**
     * metoda Zarejestruj, ktora wywolywana jest kliknieciu przycisku Zarejestruj
     * dane zapisywane sa do tabeli UZYTKOWNICY
     * metoda wyswietla odpowiedni komunikat w zaleznosci od zwroconej wartosci false/true
     */
    private void Zarejestruj(){
        btnZarejestruj.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                trySignUp();
            }

            /**
             * metoda sprawdza czy pola do wspisania e-mail, hasla, imienia i nazwiska nie sa puste
             * https://www.youtube.com/watch?v=gojZrfqio2w
             */
            void trySignUp() {
                boolean failed = false;
                String email = editAdresEmail.getText().toString();
                String haslo = editHaslo.getText().toString();
                String imie = editImie.getText().toString();
                String nazwisko = editNazwisko.getText().toString();
                String hashHaslo = "";  //  inicjalizacja zmiennej jest wymagana

                    //  warunki dla e-maila
                if(!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
                    editAdresEmail.setError("Niepoprawny adres e-mail");
                    failed = true;
                }
                else if(SprawdzUnikalnoscAdresEmail(email)){
                    editAdresEmail.setError("Taki adres e-mail już istnieje!");
                    failed = true;
                }
                else if(TextUtils.isEmpty(email)) {
                    editAdresEmail.setError("Pole adresu e-mail nie może być puste!");
                    failed = true;
                }
                else if(email.length() > 51) {
                    editAdresEmail.setError("Adres e-mail może mieć maksymalnie 50 znaków");
                    failed = true;
                }

                    //  warunki dla hasla
                if(haslo.length() < 5) {
                    editHaslo.setError("Hasło musi mieć minimum 5 znaków");
                    failed = true;
                }
                else if(haslo.length() > 31) {
                    editHaslo.setError("Hasło może mieć maksymalnie 30 znaków");
                    failed = true;
                }

                    //  warunki dla imienia
                if(TextUtils.isEmpty(imie)) {
                    editImie.setError("Pole imienia nie może być puste!");
                    failed = true;
                }
                else if(imie.length() > 21) {
                    editImie.setError("Imię może mieć maksymalnie 20 znaków");
                    failed = true;
                }
                else if(!isAlpha(imie)) {
                    editImie.setError("Imię może składać się tylko z liter");
                    failed = true;
                }

                    //  warunki dla nazwiska
                if(TextUtils.isEmpty(nazwisko)) {
                    editNazwisko.setError("Pole nazwiska nie może być puste!");
                    failed = true;
                }
                else if(nazwisko.length() > 31) {
                    editNazwisko.setError("Nazwisko może mieć maksymalnie 30 znaków");
                    failed = true;
                }
                else if(!isAlpha(nazwisko)) {
                    editNazwisko.setError("Nazwisko może składać się tylko z liter");
                    failed = true;
                }

                if(!failed) {
                    try {
                        hashHaslo = HashPassword.hashPassword(editHaslo.getText().toString());  //  hashowanie hasla uzytkowanika, aby nie bylo wyswietlane w bazie danych w postaci jawnej
                    } catch (Exception exception){                                              //  https://stackoverflow.com/questions/5162587/unhandled-exception-type-error
                        Log.e("Encryption pass error/R", exception.getMessage());
                    }

                    boolean isInserted = zdalnaRecepcjonistka.insertDataRegistration(editAdresEmail.getText().toString(), hashHaslo, editImie.getText().toString(), editNazwisko.getText().toString(), editNrTelefonu.getText().toString());

                    if(isInserted) {
                        Toast.makeText(RegistrationActivity.this, "Zarejestrowano użytkownika", Toast.LENGTH_LONG).show();
                        Intent intentSignIn = new Intent(RegistrationActivity.this, SignInActivity.class);
                        startActivity(intentSignIn);
                    }
                    else
                        Toast.makeText(RegistrationActivity.this,"Błąd przy rejestracji", Toast.LENGTH_LONG).show();
                }
            }

            /**
             * metoda sprawdza czy wprowadzone zostaly tylko litery
             * param name
             * return
             */
            boolean isAlpha(String name) {
                char[] chars = name.toCharArray();

                for (char c : chars) {
                    if(!Character.isLetter(c)) {
                        return false;
                    }
                }
                return true;
            }

            /**
             * metoda sprawdza czy w bazie danych nie istnieje adres e-mail, ktory chce podac uzytkownik
             * param Email
             * return
             */
            boolean SprawdzUnikalnoscAdresEmail(String Email) {
                Cursor res = zdalnaRecepcjonistka.checkRegistrationAddressEmail(Email);
                if(res.getCount() > 0) {
                    res.close();
                    return true;
                }
                else {
                    res.close();
                    return false;
                }
            }
        });
    }
}
