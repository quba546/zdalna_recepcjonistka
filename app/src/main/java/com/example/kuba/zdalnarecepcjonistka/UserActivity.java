package com.example.kuba.zdalnarecepcjonistka;

import android.content.Intent;
import android.database.Cursor;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import java.util.Calendar;

public class UserActivity extends AppCompatActivity {

    private DatabaseHelper zdalnaRecepcjonistka;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);

        zdalnaRecepcjonistka = new DatabaseHelper(this);

        TextView txtImie = findViewById(R.id.textViewUserImieIcon);
        TextView txtNazwisko = findViewById(R.id.textViewUserNazwiskoIcon);
        TextView txtEmail = findViewById(R.id.textViewUserEmailIcon);
        TextView txtDataRejestracji = findViewById(R.id.textViewUserDataRejestracji);

        try {
            Cursor res = zdalnaRecepcjonistka.pobierzDaneUzytkownika(SignInActivity.idUser);
            if (res != null && res.moveToFirst()) {
                txtEmail.setText(res.getString(0));
                txtImie.setText(res.getString(1));
                txtNazwisko.setText(res.getString(2));
                txtDataRejestracji.setText(res.getString(3));
            }
            assert res != null;
            res.close();
        }
        catch (Exception exception) {
            Log.e("Get user's data error/U", exception.getMessage());
        }
    }

    /**
     * metoda pobiera dane o historii rezerwacji uzytkownika
     * param view
     */
    public void clickUser(View view) {
        switch (view.getId()){
            case R.id.buttonZlozRezerwacje:
                Intent intentReservation = new Intent(UserActivity.this, ReservationActivity.class);
                startActivity(intentReservation);
                break;
            case R.id.buttonSprawdzHistorieRezerwacji:
                Cursor res = zdalnaRecepcjonistka.pobierzHistorieRezerwacji(SignInActivity.idUser);
                if(res.getCount() == 0)
                    showMessage("Brak rezerwacji", "Nie znaleziono żadnej rezerwacji");
                else {
                    StringBuilder buffer = new StringBuilder();
                    while (res.moveToNext()) {
                        int sDay = milisToDate(res.getLong(2)* 1000 * 60 * 60 * 24)[0];
                        int sMonth = milisToDate(res.getLong(2)* 1000 * 60 * 60 * 24)[1];
                        int sYear = milisToDate(res.getLong(2)* 1000 * 60 * 60 * 24)[2];
                        int fDay = milisToDate(res.getLong(3)* 1000 * 60 * 60 * 24)[0];
                        int fMonth = milisToDate(res.getLong(3)* 1000 * 60 * 60 * 24)[1];
                        int fYear = milisToDate(res.getLong(3)* 1000 * 60 * 60 * 24)[2];

                        buffer.append("Numer rezerwacji: ").append(res.getInt(0)).append("\n");
                        buffer.append("Data rezerwacji: ").append(res.getString(1)).append("\n");
                        buffer.append("Rezerwacja od ").append(sDay).append("/").append(sMonth + 1).append("/").append(sYear).append(" do ").append(fDay).append("/").append(fMonth + 1).append("/").append(fYear).append("\n");
                        buffer.append("Numer pokoju: ").append(res.getInt(4)).append("\n");
                        buffer.append("Cena: ").append(res.getLong(5)).append(" zł\n");
                        buffer.append("Kod: ").append(res.getString(6)).append("\n");
                        buffer.append("Pobrano klucz: ").append(res.getString(7)).append("\n");
                        buffer.append("Zwrócono klucz: ").append(res.getString(8)).append("\n\n");
                    }
                    showMessage("Twoje rezerwacje", buffer.toString());
                }
                res.close();
                break;
            case R.id.buttonWyloguj:
                SignInActivity.idUser = 0;

                Intent intent = new Intent(this, MainActivity.class);   // New activity
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();   // Call once you redirect to another activity
                break;
        }
    }

    /**
     * metoda zamienia milisekundy na date, zwracajac teblice int zawierajaca dzien, miesiac, rok
     * miesiace zaczynaja sie od 0
     * param miliseconds
     * return
     */
    private int[] milisToDate(long miliseconds) {

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(miliseconds);

        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        int[] date;
        date = new int[] {day, month, year};

        return date;
    }

    /**
     * metoda wyswitla wiadomosc z historia rezerwacji uzytkownika
     * param title
     * param Message
     */
    private void showMessage(String title, String Message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setTitle(title);
        builder.setMessage(Message);
        builder.show();
    }
}
