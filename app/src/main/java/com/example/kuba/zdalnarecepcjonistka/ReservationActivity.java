package com.example.kuba.zdalnarecepcjonistka;


import android.annotation.SuppressLint;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;


public class ReservationActivity extends AppCompatActivity {

    private DatabaseHelper zdalnaRecepcjonistka;

    private RadioButton rbPokoj1;
    private RadioButton rbPokoj2;
    private RadioButton rbPokoj3;
    private TextView wybierzStartDate;
    private TextView wybierzFinishDate;
    private TextView ileDni;
    private TextView jakaCena;
    private Calendar startCalendar;
    private Calendar finishCalendar;
    private Button btnZlozRezerwacje;
    private final Handler handler = new Handler();

    private int id_pokoju;      // dla zmiennej typu int domyslna wartoscia jest 0
    private final int[] id_uslugi = new int[5];
    private int id_reservation;
    private long cena_pokoju;
    private final long[] cena_uslug = new long[5];
    private String kod;
    private boolean wolnyTermin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reservation);

        zdalnaRecepcjonistka = new DatabaseHelper(this);

        //  pobieranie danych do wypelnienia pol na stronie rezerwacji
        TextView viewImie = findViewById(R.id.textViewReservationImie);
        TextView viewNazwisko = findViewById(R.id.textViewReservationNazwisko);
        TextView viewEmail = findViewById(R.id.textViewReservationEmail);
        wybierzStartDate = findViewById(R.id.textViewInputDataPoczatku);
        wybierzFinishDate = findViewById(R.id.textViewInputDataKonca);

        try {
            Cursor res = zdalnaRecepcjonistka.pobierzDaneUzytkownika(SignInActivity.idUser);
            if (res != null && res.moveToFirst()) {
                viewEmail.setText(res.getString(0));
                viewImie.setText(res.getString(1));
                viewNazwisko.setText(res.getString(2));
            }
            assert res != null;
            res.close();
        }
        catch (Exception exception) {
            Log.e("Get user's data error", exception.getMessage());
        }

        // obsluga Radio Buttonow
        rbPokoj1 = findViewById(R.id.radioButtonPokoj1);
        rbPokoj2 = findViewById(R.id.radioButtonPokoj2);
        rbPokoj3 = findViewById(R.id.radioButtonPokoj3);

        // obsluga przycisku Zloz rezerwacje
        btnZlozRezerwacje = findViewById(R.id.buttonZlozRezerwacje);
        ileDni = findViewById(R.id.textViewIleDni);
        jakaCena = findViewById(R.id.textViewCena);

        wybierzPokoj();
        ZlozRezerwacje();
    }

    /**
     * obsluga wyboru pokoju
     */
    private void wybierzPokoj() {

        rbPokoj1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                id_pokoju = 1;
                wybierzDatePoczatkuKoncaRezerwacji();
                handler.sendEmptyMessage(0);
            }
        });
        rbPokoj2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                id_pokoju = 2;
                wybierzDatePoczatkuKoncaRezerwacji();
                handler.sendEmptyMessage(0);
            }
        });
        rbPokoj3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                id_pokoju = 3;
                wybierzDatePoczatkuKoncaRezerwacji();
                handler.sendEmptyMessage(0);
            }
        });
    }

    private void wybierzDatePoczatkuKoncaRezerwacji() {
        Cursor result = zdalnaRecepcjonistka.pobierzZajeteDni(id_pokoju);   // pobieranie i blokowanie zajetych dni w kalendarzu

        //  blokowanie dat w kalendarzu
        List<Calendar> list= new ArrayList<>();
        result.moveToFirst();
        for(int i = 0; i < result.getCount(); i++) {

            for(long j = result.getLong(0); j <= result.getLong(1); j++) {
                Calendar calendar = Calendar.getInstance();
                calendar.setTimeInMillis(j * 24 * 60 * 60 * 1000);
                list.add(calendar);
            }

            result.moveToNext();
        }
        result.close();

        final Calendar[] zajete_dni = new Calendar[list.size()];
        for(int i = 0; i < list.size(); i++) {
            zajete_dni[i] = list.get(i);
        }

        startCalendar = Calendar.getInstance();
        final int sDay = startCalendar.get(Calendar.DAY_OF_MONTH);
        final int sMonth = startCalendar.get(Calendar.MONTH);
        final int sYear = startCalendar.get(Calendar.YEAR);

        finishCalendar = Calendar.getInstance();
        final int fDay = finishCalendar.get(Calendar.DAY_OF_MONTH);
        final int fMonth = finishCalendar.get(Calendar.MONTH);
        final int fYear = finishCalendar.get(Calendar.YEAR);

        com.wdullaer.materialdatetimepicker.date.DatePickerDialog startDatePickerDialog = com.wdullaer.materialdatetimepicker.date.DatePickerDialog.newInstance(new com.wdullaer.materialdatetimepicker.date.DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(com.wdullaer.materialdatetimepicker.date.DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {

                String strStartDate = dayOfMonth + "/" + (monthOfYear + 1) + "/" + year;
                wybierzStartDate.setText(strStartDate);
                startCalendar.set(year, monthOfYear, dayOfMonth);

                com.wdullaer.materialdatetimepicker.date.DatePickerDialog finishDatePickerDialog = com.wdullaer.materialdatetimepicker.date.DatePickerDialog.newInstance(new com.wdullaer.materialdatetimepicker.date.DatePickerDialog.OnDateSetListener() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onDateSet(com.wdullaer.materialdatetimepicker.date.DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {

                        String strFinishDate = dayOfMonth + "/" + (monthOfYear + 1) + "/" + year;
                        wybierzFinishDate.setText(strFinishDate);
                        finishCalendar.set(year, monthOfYear, dayOfMonth);

                        if(zdalnaRecepcjonistka.sprawdzDostepnoscPokoju(id_pokoju, startCalendar.getTimeInMillis() /1000/60/60/24, finishCalendar.getTimeInMillis() /1000/60/60/24) > 0) {
                            AlertDialog alert;
                            AlertDialog.Builder builder = new AlertDialog.Builder(ReservationActivity.this);     // wyswietlanie okna dialogowego
                            alert = builder.create();
                            alert.setCancelable(true);
                            alert.setTitle("Zajęty termin rezerwacji");
                            alert.setMessage("Termin rezerwacji, który wybrałeś jest już zajęty. Wybierz inną datę.");
                            alert.show();
                            wolnyTermin = false;
                        }
                        else {
                            ileDni.setText(Long.toString(daysBetween(startCalendar, finishCalendar)));
                            aktualizacjaCenySumarycznej();
                            wolnyTermin = true;
                        }
                    }
                }, fYear, fMonth, fDay);

                Calendar minCalendar = Calendar.getInstance();
                minCalendar.set(startCalendar.get(Calendar.YEAR), startCalendar.get(Calendar.MONTH), startCalendar.get(Calendar.DAY_OF_MONTH) + 1);
                finishDatePickerDialog.setMinDate(minCalendar);
                finishDatePickerDialog.setDisabledDays(zajete_dni);
                finishDatePickerDialog.setTitle("Wybierz końcową datę");
                finishDatePickerDialog.show(getFragmentManager(), "DatePickerDialog");
            }
        }, sYear, sMonth, sDay);

        startDatePickerDialog.setMinDate(startCalendar);
        startDatePickerDialog.setDisabledDays(zajete_dni);
        startDatePickerDialog.setTitle("Wybierz datę początkową");
        startDatePickerDialog.show(getFragmentManager(), "DatePickerDialog");

        Cursor resultCenaPokoju = zdalnaRecepcjonistka.pobierzCenePokoju(id_pokoju);
            if (resultCenaPokoju != null && resultCenaPokoju.moveToFirst())
                cena_pokoju = resultCenaPokoju.getInt(0);
        assert resultCenaPokoju != null;
        resultCenaPokoju.close();
    }

    /**
     * obsluga wyboru uslug
     * param view
     */
    public void onCheckboxClicked(View view) {

        boolean checked = ((CheckBox) view).isChecked();

        switch (view.getId()) {
            case R.id.checkBoxSniadanie:
                if(checked)
                    id_uslugi[0] = 1;
                else
                    id_uslugi[0] = 0;
                aktualizacjaCenySumarycznej();
                break;
            case R.id.checkBoxPelneWyzywienie:
                if(checked)
                    id_uslugi[1] = 2;
                else
                    id_uslugi[1] = 0;
                aktualizacjaCenySumarycznej();
                break;
            case R.id.checkBoxBasen:
                if(checked)
                    id_uslugi[2] = 3;
                else
                    id_uslugi[2] = 0;
                aktualizacjaCenySumarycznej();
                break;
            case R.id.checkBoxSauna:
                if(checked)
                    id_uslugi[3] = 4;
                else
                    id_uslugi[3] = 0;
                aktualizacjaCenySumarycznej();
                break;
            case R.id.checkBoxHalaSportowa:
                if(checked)
                    id_uslugi[4] = 5;
                else
                    id_uslugi[4] = 0;
                aktualizacjaCenySumarycznej();
                break;
        }
    }

    /**
     * metoda aktualizuje cene sumaryczna
     */
    @SuppressLint("SetTextI18n")
    private void aktualizacjaCenySumarycznej() {
        for (int i = 0; i < id_uslugi.length; i++) {
            if (id_uslugi[i] != 0) {
                Cursor resultCenaUslug = zdalnaRecepcjonistka.pobierzCeneUslugi(id_uslugi[i]);
                if (resultCenaUslug != null && resultCenaUslug.moveToFirst())
                    cena_uslug[i] = resultCenaUslug.getInt(0);
                assert resultCenaUslug != null;
                resultCenaUslug.close();
            } else
                cena_uslug[i] = 0;
        }
        if(cena_pokoju != 0)
            jakaCena.setText(Long.toString(obliczCene(daysBetween(startCalendar, finishCalendar), cena_pokoju, cena_uslug)) + " zł"); //    wpisywanie ceny sumarycznej do Texview
    }

    /**
     * funkcja oblicza ilosc dni miedzy dwoma datami
     * param startDate
     * param endDate
     * return
     */
    private static long daysBetween(Calendar startDate, Calendar endDate) {
        long diff = endDate.getTimeInMillis() - startDate.getTimeInMillis();

        return (diff / (1000 * 60 * 60 * 24)); //   1000 milisekund w sekundzie * 60 sekund w minucie * 60 minut w godzinie * 24 godziny w dniu
    }

    /**
     * funkcja oblicza lacza cene za wynajem
     * param ilosc_dni
     * param cena_pokoju
     * param cena_ulsug
     * return
     */
    private static long obliczCene(long ilosc_dni, long cena_pokoju, long cena_ulsug[]) {
        long cena_koncowa = 0;
        cena_koncowa += cena_pokoju;

        for(int i = 0; i < cena_ulsug.length; i++) {
            cena_koncowa += cena_ulsug[i];
        }

        cena_koncowa *= ilosc_dni;

        return cena_koncowa;
    }

    /**
     * funkcja wywolana zostaje poprzez nacisniecie przycisku 'Zloz rezerwacje'
     */
    private void ZlozRezerwacje() {
        btnZlozRezerwacje.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(wolnyTermin) {
                    RandomString rs = new RandomString();   // generowanie pseudolosowego kodu
                    kod = rs.randomString(8);

                    boolean resRezerwacje = zdalnaRecepcjonistka.dodajRezerwacje(SignInActivity.idUser, startCalendar.getTimeInMillis() / 1000 / 60 / 60 / 24, finishCalendar.getTimeInMillis() / 1000 / 60 / 60 / 24, id_pokoju, obliczCene(daysBetween(startCalendar, finishCalendar), cena_pokoju, cena_uslug), kod);
                    boolean resRezerwacjeUslugi = zdalnaRecepcjonistka.dodajRezerwacjeUslugi(id_reservation, id_uslugi);

                    if (resRezerwacje && resRezerwacjeUslugi) {
                        Cursor res = zdalnaRecepcjonistka.pobierzIdRezerwacji(startCalendar.getTimeInMillis() / 1000 / 60 / 60 / 24, finishCalendar.getTimeInMillis() / 1000 / 60 / 60 / 24, id_pokoju);
                        if (res != null && res.moveToFirst())
                            id_reservation = res.getInt(0);
                        assert res != null;
                        res.close();

                        SendEmail wyslijEmail = new SendEmail();    //  wysylanie e-maila z kodem
                        wyslijEmail.execute();

                        for (int i = 0; i < id_uslugi.length; i++)  //  zerowanie tablicy zmiennych globalnych id_uslugi
                            id_uslugi[i] = 0;
                        for (int i = 0; i < cena_uslug.length; i++) //  zerowanie tablicy zmiennych globalnych cena_uslug
                            cena_uslug[i] = 0;

                        Intent intent = new Intent(ReservationActivity.this, UserActivity.class);   //  wywoalnie nowej aktywnosci po poprawnym zlozniu rezerwacji
                        Toast.makeText(ReservationActivity.this, "Dodano rezerwację", Toast.LENGTH_SHORT).show();
                        startActivity(intent);
                    }
                    else
                        Toast.makeText(ReservationActivity.this, "Wystąpił problem z dodaniem rezerwacji", Toast.LENGTH_SHORT).show();
                }
                else
                    Toast.makeText(ReservationActivity.this, "Wybierz inny termin rezerwacji", Toast.LENGTH_SHORT).show();
            }
        });
    }

    /**
     * klasa odpowiedzialna za wysylanie e-maila
     */
    @SuppressLint("StaticFieldLeak")
    class SendEmail extends AsyncTask<String, Integer, Long> {


        protected Long doInBackground(String... body) {
            String email = "";
            try{
                try {
                    Cursor res = zdalnaRecepcjonistka.pobierzDaneUzytkownika(SignInActivity.idUser);
                    if (res != null && res.moveToFirst()) {
                        email = res.getString(0);
                    }
                    assert res != null;
                    res.close();
                } catch (Exception e) {
                    Log.e("Get e-mail error", e.getMessage());
                }

                GMailSender sender = new GMailSender("zdalna.recepcjonistka@gmail.com", "zdalna123");
                sender.sendMail("Rezerwacja nr " + id_reservation, "Witaj,\n\nzłożyłeś rezerwację przez aplikację Zdalna recepcjonistka. Wybrałeś termin " +
                                "rezerwacji od " + startCalendar.get(Calendar.DAY_OF_MONTH) + "-" + (startCalendar.get(Calendar.MONTH) + 1) + "-" + startCalendar.get(Calendar.YEAR) +
                                " do " + finishCalendar.get(Calendar.DAY_OF_MONTH) + "-" + (finishCalendar.get(Calendar.MONTH) + 1) + "-" + finishCalendar.get(Calendar.YEAR) + " dla pokoju " + id_pokoju + ". Cena za wynajem tego pokoju oraz wybranych usług przez " + daysBetween(startCalendar, finishCalendar) +
                                " dni wynosi " + obliczCene(daysBetween(startCalendar, finishCalendar), cena_pokoju, cena_uslug) + " zł.\n" +
                                "Poniżej znajduje się kod, który będzie ci potrzebny do pobrania klucza do pokoju z szafki:\nTwój kod: " + kod + "\nŻyczymy miłego pobytu!\n\n\n" +
                                "Zespół Zdalna recepcjonistka",
                        "zdalna.recepcjonistka@gmail.com",
                        email);
            } catch (Exception e) {
                Log.e("Mail send error", e.getMessage());
            }
            return null;
        }

        protected void onProgressUpdate(Integer... progress) {
        }

        protected void onPostExecute(Long result) {
            Toast.makeText(getApplicationContext(), "Wiadomość została wysłana", Toast.LENGTH_SHORT).show();
        }
    }
}