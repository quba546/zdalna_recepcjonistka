package com.example.kuba.zdalnarecepcjonistka;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Environment;

import java.io.File;
import java.text.DateFormat;
import java.util.Date;

class DatabaseHelper extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "Zdalna_recepcjonistka.db";
    private static final String FILE_DIR = "Ringtones";

    /**
     * TABELA UZYTKOWNICY
     */
    private static final String TABLE1_NAME = "UZYTKOWNICY";
    private static final String UZYTKOWNICY_COL1 = "id_uzytkownika";
    private static final String UZYTKOWNICY_COL2 = "adres_email";
    private static final String UZYTKOWNICY_COL3 = "haslo";
    private static final String UZYTKOWNICY_COL4 = "imie";
    private static final String UZYTKOWNICY_COL5 = "nazwisko";
    private static final String UZYTKOWNICY_COL6 = "nr_telefonu";
    private static final String UZYTKOWNICY_COL7 = "data_rejestracji";

    /**
     * TABELA REZERWACJE
     */
    private static final String TABLE2_NAME = "REZERWACJE";
    private static final String REZERWACJE_COL1 = "id_rezerwacji";
    private static final String REZERWACJE_COL2 = "id_uzytkownika";
    private static final String REZERWACJE_COL3 = "data_zlozenia_rezerwacji";
    private static final String REZERWACJE_COL4 = "poczatek_rezerwacji";
    private static final String REZERWACJE_COL5 = "koniec_rezerwacji";
    private static final String REZERWACJE_COL6 = "id_pokoju";
    private static final String REZERWACJE_COL7 = "cena_sumaryczna";
    private static final String REZERWACJE_COL8 = "kod";
    private static final String REZERWACJE_COL9 = "pobrano_klucz";
    private static final String REZERWACJE_COL10 = "zwrocono_klucz";

    /**
     * TABELA POKOJE
     */
    private static final String TABLE3_NAME = "POKOJE";
    private static final String POKOJE_COL1 = "id_pokoju";
    private static final String POKOJE_COL2 = "ilosc_lozek";
    private static final String POKOJE_COL3 = "cena_pokoju";

    /**
     * TABELA USLUGI
     */
    private static final String TABLE4_NAME = "USLUGI";
    private static final String USLUGI_COL1 = "id_uslugi";
    private static final String USLUGI_COL2 = "nazwa_uslugi";
    private static final String USLUGI_COL3 = "cena_uslugi";

    /**
     * TABELA REZERWACJE_USLUGI
     */
    private static final String TABLE5_NAME = "REZERWACJE_USLUGI";
    private static final String REZERWACJE_USLUGI_COL1 = "id_rezerwacji";
    private static final String REZERWACJE_USLUGI_COL2 = "id_uslugi";

    /**
     * metoda zapisuje plik bazy danych w odpowiedniej lokalizacji
     * param context
     */
    DatabaseHelper(Context context) {
        super(context, Environment.getExternalStorageDirectory() + File.separator + FILE_DIR + File.separator + DATABASE_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        //  utworzenie tabel
        db.execSQL("create table " + TABLE1_NAME + " (id_uzytkownika integer primary key autoincrement not null, adres_email text not null, haslo text not null, imie text not null, nazwisko text not null, nr_telefonu text, data_rejestracji text not null)");
        db.execSQL("create table " + TABLE2_NAME + " (id_rezerwacji integer primary key autoincrement not null, id_uzytkownika integer not null, data_zlozenia_rezerwacji text, poczatek_rezerwacji integer not null, koniec_rezerwacji integer not null, id_pokoju integer not null, cena_sumaryczna integer, kod text not null, pobrano_klucz text default 'NIE', zwrocono_klucz text default 'NIE')");
        db.execSQL("create table " + TABLE3_NAME + " (id_pokoju integer primary key autoincrement not null, ilosc_lozek integer not null, cena_pokoju integer not null)");
        db.execSQL("create table " + TABLE4_NAME + " (id_uslugi integer primary key autoincrement not null, nazwa_uslugi text not null, cena_uslugi integer not null)");
        db.execSQL("create table " + TABLE5_NAME + " (id_rezerwacji integer not null, id_uslugi integer not null)");

        //  dodanie konta administratora
        db.execSQL("insert into " + TABLE1_NAME + "(adres_email, haslo, imie, nazwisko, nr_telefonu, data_rejestracji) values ('zdalna.recepcjonistka@gmail.com', '21232f297a57a5a743894ae4a801fc3', 'admin', 'admin', '123456789', '" + DateFormat.getDateTimeInstance().format(new Date()) + "')");

        //  dodanie pokoi do tabeli POKOJE
        db.execSQL("insert into " + TABLE3_NAME + "(ilosc_lozek, cena_pokoju) values ('4', '180')");
        db.execSQL("insert into " + TABLE3_NAME + "(ilosc_lozek, cena_pokoju) values ('6', '250')");
        db.execSQL("insert into " + TABLE3_NAME + "(ilosc_lozek, cena_pokoju) values ('8', '360')");

        //  dodanie uslug do tabeli USLUGI
        db.execSQL("insert into " + TABLE4_NAME + "(nazwa_uslugi, cena_uslugi) values ('Śniadanie (cena za dzień)', '20')");
        db.execSQL("insert into " + TABLE4_NAME + "(nazwa_uslugi, cena_uslugi) values ('Pełne wyżywienie (cena za dzień)', '50')");
        db.execSQL("insert into " + TABLE4_NAME + "(nazwa_uslugi, cena_uslugi) values ('Basen (cena za dzień)', '25')");
        db.execSQL("insert into " + TABLE4_NAME + "(nazwa_uslugi, cena_uslugi) values ('Sauna (cena za dzień)', '15')");
        db.execSQL("insert into " + TABLE4_NAME + "(nazwa_uslugi, cena_uslugi) values ('Hala sportowa (cena za dzień)', '80')");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("drop table if exists " + TABLE1_NAME);
        db.execSQL("drop table if exists " + TABLE2_NAME);
        db.execSQL("drop table if exists " + TABLE3_NAME);
        db.execSQL("drop table if exists " + TABLE4_NAME);
        onCreate(db);
    }

    /**
     * metoda zapisuje dane pobrane z pol tekstowych do tabeli
     * zwraca -1 jesli wpisanie do tabeli nie powiodlo sie
     * param adres_email
     * param haslo
     * param imie
     * param nazwisko
     * param nr_telefonu
     * return
     */
    boolean insertDataRegistration(String adres_email, String haslo, String imie, String nazwisko, String nr_telefonu){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(UZYTKOWNICY_COL2, adres_email);
        contentValues.put(UZYTKOWNICY_COL3, haslo);
        contentValues.put(UZYTKOWNICY_COL4, imie);
        contentValues.put(UZYTKOWNICY_COL5, nazwisko);
        contentValues.put(UZYTKOWNICY_COL6, nr_telefonu);
        contentValues.put(UZYTKOWNICY_COL7, DateFormat.getDateTimeInstance().format(new Date()));
        long result = db.insert(TABLE1_NAME, null, contentValues);
        if(result == -1) {
            db.close();
            return false;
        }
        else {
            db.close();
            return true;
        }
    }

    /**
     * metoda wyszukuje z tabeli UZYTKOWNICY adresy e-mail podane przez uzytkownika podczas rejestracji
     * param email
     * return
     */
    Cursor checkRegistrationAddressEmail(String email) {
        SQLiteDatabase db = this.getWritableDatabase();

        return db.rawQuery("select " + UZYTKOWNICY_COL2 + " from " + TABLE1_NAME + " where " + UZYTKOWNICY_COL2 + " = '" + email + "'" , null);
    }

    /**
     * metoda sprawdza czy w tabeli UZYTKONIWCY wystepuje podany przy probie zalogowanie adres e-mail
     * param email
     * return
     */
    Cursor checkSignInAddressEmail(String email) {
        SQLiteDatabase db = this.getWritableDatabase();

        return db.rawQuery("select " + UZYTKOWNICY_COL2 + " from " + TABLE1_NAME + " where " + UZYTKOWNICY_COL2 + " = '" + email + "'" , null);
    }

    /**
     * metoda sprawdza czy w tabeli UZYTKONIWCY wystepuje podane przy probie zalogowania haslo
     * param password
     * return
     */
    Cursor checkSignInPassword(String password) {
        SQLiteDatabase db = this.getWritableDatabase();

        return db.rawQuery("select " + UZYTKOWNICY_COL3 + " from " + TABLE1_NAME + " where " + UZYTKOWNICY_COL3 + " = '" + password + "'" , null);
    }

    /**
     * metoda pobiera dane id_uzytkownika z tabeli UZYTKOWNICY
     * param email
     * param haslo
     * return
     */
    Cursor pobierzIdUzytkownika(String email, String haslo) {
        SQLiteDatabase db = this.getWritableDatabase();

        return db.rawQuery("select " + UZYTKOWNICY_COL1 + " from " + TABLE1_NAME + " where " + UZYTKOWNICY_COL2 + " = '" + email + "' and " + UZYTKOWNICY_COL3 + " = '" + haslo + "'", null);
    }

    /**
     * metoda pobiera dane imie, nazwisko z tabeli UZYTKOWNICY
     * param id_user
     * return
     */
    Cursor pobierzDaneUzytkownika(int id_user) {
        SQLiteDatabase db = this.getWritableDatabase();

        return db.rawQuery("select " + UZYTKOWNICY_COL2 + ", " + UZYTKOWNICY_COL4 + ", " + UZYTKOWNICY_COL5 + ", " + UZYTKOWNICY_COL7 + " from " + TABLE1_NAME + " where " + UZYTKOWNICY_COL1 + " = '" + id_user + "'", null);
    }

    /**
     * metoda dodajaca rezerwacje w tabeli REZERWACJE
     * param id_user
     * param start_reservation
     * param end_reservation
     * param id_room
     * param total_price
     * param code
     * return
     */
    boolean dodajRezerwacje(int id_user, long start_reservation, long end_reservation, int id_room, long total_price, String code) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(REZERWACJE_COL2, id_user);
        contentValues.put(REZERWACJE_COL3, DateFormat.getDateTimeInstance().format(new Date()));
        contentValues.put(REZERWACJE_COL4, start_reservation);
        contentValues.put(REZERWACJE_COL5, end_reservation);
        contentValues.put(REZERWACJE_COL6, id_room);
        contentValues.put(REZERWACJE_COL7, total_price);
        contentValues.put(REZERWACJE_COL8, code);
        long result = db.insert(TABLE2_NAME, null, contentValues);
        if(result == -1) {
            db.close();
            return false;
        }
        else {
            db.close();
            return true;
        }
    }

    /**
     * metoda dodaje informacje do tabeli REZERWACJE o pobraniu kluczy tzn. drugi raz nie bedzie mozliwe
     * pobranie klucza dla tej rezerwacji
     * param kod
     * return
     */
    void dodajPobranieKlucza(String kod) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("update " + TABLE2_NAME + " set " + REZERWACJE_COL9 + " = 'TAK' where " + REZERWACJE_COL8 + " = '" + kod + "'");
        db.close();
    }

    void dodajZwrocenieKlucza(String kod) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("update " + TABLE2_NAME + " set " + REZERWACJE_COL10 + " = 'TAK' where " + REZERWACJE_COL8 + " = '" + kod + "'");
        db.close();
    }

    /**
     * metoda pobiera dane id_rezerwacji z tabeli REZERWACJE
     * na podstawie daty poczatku, konca rezerwacji oraz id pokoju
     * pobrane zostaje id rezerwacji z tabeli REZERWACJE
     * param data_poczatek
     * param data_koniec
     * param id_room
     * return
     */
    Cursor pobierzIdRezerwacji(long data_poczatek, long data_koniec, int id_room) {
        SQLiteDatabase db = this.getWritableDatabase();

        return db.rawQuery("select " + REZERWACJE_COL1 + " from " + TABLE2_NAME + " where " + REZERWACJE_COL4 + " = '" + data_poczatek + "' and " + REZERWACJE_COL5 + " = '" + data_koniec + "' and " + REZERWACJE_COL6 + " = '" + id_room + "'", null);
    }

    /**
     * metoda dodajaca rezerwacje w tabeli REZERWACJE_USLUGI
     * jako arugmenty podane sa id rezerwacji oraz id uslugi jako tablica
     * tablica zostaje przeszukana w petli for od 0 do dlugosci tablicy,
     * jesli wartosc w tablicy jest rozna od 0 to do tabeli REZERWACJE_USLUGI
     * zostaja dodane wartosci id rezerwacji oraz przypisana mu usluge
     * jedna rezerwacja moze posiadac kilka uslug
     * param id_reservation
     * param id_service
     * return
     */
    boolean dodajRezerwacjeUslugi(int id_reservation, int id_service[]) {
        SQLiteDatabase db = this.getWritableDatabase();
        long result = 0;
        ContentValues contentValues = new ContentValues();
        for(int i = 0; i < id_service.length; i++) {
            if (id_service[i] != 0) {
                contentValues.put(REZERWACJE_USLUGI_COL1, id_reservation);
                contentValues.put(REZERWACJE_USLUGI_COL2, id_service[i]);
                result = db.insert(TABLE5_NAME, null, contentValues);
            }
        }
        if(result == -1) {
            db.close();
            return false;
        }
        else {
            db.close();
            return true;
        }
    }

    /**
     * metoda pobiera cene pokoju z tabeli POKOJE
     * param id_room
     * return
     */
    Cursor pobierzCenePokoju(int id_room) {
        SQLiteDatabase db = this.getWritableDatabase();

        return db.rawQuery("select " + POKOJE_COL3 + " from " + TABLE3_NAME + " where " + POKOJE_COL1 + " = '" + id_room + "'", null);
    }

    /**
     * metoda pobiera cene uslugi z tablei USLUGI
     * param id_service
     * return
     */
    Cursor pobierzCeneUslugi(int id_service) {
        SQLiteDatabase db = this.getWritableDatabase();

        return db.rawQuery("select " + USLUGI_COL3 + " from " + TABLE4_NAME + " where " + USLUGI_COL1 + " = '" + id_service + "'", null);
    }

    /**
     * metoda pobiera daty poczatku i konca rezerwacje z tabeli REZERWACJE dla konkretengo pokoju
     * wpisany warunek w poleceinu SQL wybiera z tabeli REZRWACJE odpowiednie wiersze
     * jesli nie znajdzie zadnego wiersza spelniajacego warunek zwraca 0,
     * w przeciwnym wypadku zwraca ilosc znalezionych wierszy
     * param id_room
     * param data_poczatku
     * param data_konca
     * return
     */
    int sprawdzDostepnoscPokoju(int id_room, long data_poczatku, long data_konca) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor res = db.rawQuery("select " + REZERWACJE_COL4 + "," + REZERWACJE_COL5 + " from " + TABLE2_NAME + " where " + REZERWACJE_COL6 + " = '" + id_room + "' and (" + REZERWACJE_COL4 + " <= '" + data_konca + "' and " + REZERWACJE_COL5 + " >= '" + data_poczatku + "')", null);
        int ilosc_wynikow = res.getCount();
        res.close();

        return ilosc_wynikow;
    }

    /**
     * metoda sprawdza czy istnieje rezerwacja z podanym adresem e-mail oraz kodem, jesli tak to nastepnie sprawdza czy klucz nie zostal juz wczesniej pobrany,
     * jesli nie to zwraca id pokoju, jesli zostal juz pobrany to zwraca -2, jesli adres e-mail byl niepoprawny lub kod to zwraca -1
     * param email
     * param code
     * return
     */
    int pobierzIdPokojuDlaKluczaReceive(String email, String code) {
        SQLiteDatabase db = this.getWritableDatabase();
        int user_id = -1;
        int id_pokoju = 0;
        String pobrano_klucz = "";

        Cursor resUser = db.rawQuery("select " + UZYTKOWNICY_COL1 + " from " + TABLE1_NAME + " where " + UZYTKOWNICY_COL2 + " = '" + email + "'", null);
        if (resUser != null && resUser.moveToFirst())
            user_id = resUser.getInt(0);
        assert resUser != null;
        resUser.close();

        if(user_id > -1) {
            Cursor resReservation = db.rawQuery("select " + REZERWACJE_COL6 + ", " + REZERWACJE_COL9 + " from " + TABLE2_NAME + " where " + REZERWACJE_COL2 + " = '" + user_id + "' and " + REZERWACJE_COL8 + " = '" + code + "'", null);
            if(resReservation != null && resReservation.moveToFirst()) {
                id_pokoju = resReservation.getInt(0);
                pobrano_klucz = resReservation.getString(1);
            }
            assert resReservation != null;
            resReservation.close();
        }

        if(id_pokoju > 0 && id_pokoju < 4)
            if(pobrano_klucz.equals("NIE"))
                return id_pokoju;
            else
                return -2;
        else
            return -1;
    }

    /**
     * metoda sprawdza czy istnieje rezerwacja z podanym adresem e-mail oraz kodem, jesli tak to nastepnie sprawdza czy klucz nie zostal juz wczesniej oddany,
     * jesli nie to zwraca id pokoju, jesli zostal juz pobrany to zwraca -2, jesli adres e-mail byl niepoprawny lub kod to zwraca -1
     * param email
     * param code
     * return
     */
    int pobierzIdPokojuDlaKluczaReturn(String email, String code) {
        SQLiteDatabase db = this.getWritableDatabase();
        int user_id = -1;
        int id_pokoju = 0;
        String zwrocono_klucz = "";

        Cursor resUser = db.rawQuery("select " + UZYTKOWNICY_COL1 + " from " + TABLE1_NAME + " where " + UZYTKOWNICY_COL2 + " = '" + email + "'", null);
        if (resUser != null && resUser.moveToFirst())
            user_id = resUser.getInt(0);
        assert resUser != null;
        resUser.close();

        if(user_id > -1) {
            Cursor resReservation = db.rawQuery("select " + REZERWACJE_COL6 + ", " + REZERWACJE_COL10 + " from " + TABLE2_NAME + " where " + REZERWACJE_COL2 + " = '" + user_id + "' and " + REZERWACJE_COL8 + " = '" + code + "'", null);
            if(resReservation != null && resReservation.moveToFirst()) {
                id_pokoju = resReservation.getInt(0);
                zwrocono_klucz = resReservation.getString(1);
            }
            assert resReservation != null;
            resReservation.close();
        }

        if(id_pokoju > 0 && id_pokoju < 4)
            if(zwrocono_klucz.equals("NIE"))
                return id_pokoju;
            else
                return -2;
        else
            return -1;
    }

    /**
     * metoda pobiera dane o rezerwacjach uzytkownika na podstawie id uzytkownika
     * param id_user
     * return
     */
    Cursor pobierzHistorieRezerwacji(int id_user) {
        SQLiteDatabase db = this.getWritableDatabase();

        return db.rawQuery("select " + REZERWACJE_COL1 + ", " + REZERWACJE_COL3 + ", " + REZERWACJE_COL4 + ", " + REZERWACJE_COL5 + ", " + REZERWACJE_COL6 + ", " + REZERWACJE_COL7 + ", " + REZERWACJE_COL8 + ", " + REZERWACJE_COL9 + ", " + REZERWACJE_COL10 + " from " + TABLE2_NAME + " where " + REZERWACJE_COL2 + " = '" + id_user + "'", null);
    }

    /**
     * metoda pobiera z tabeli REZERWACJE daty poczatku i konca rezerwacji dla wybranego pokoju
     * param pokoj
     * return
     */
    Cursor pobierzZajeteDni(int pokoj) {
        SQLiteDatabase db = this.getWritableDatabase();

        return db.rawQuery("select " + REZERWACJE_COL4 + ", " + REZERWACJE_COL5 + " from " + TABLE2_NAME + " where " + REZERWACJE_COL6 + " = " + pokoj + " order by " + REZERWACJE_COL4 + " asc", null);
    }
}