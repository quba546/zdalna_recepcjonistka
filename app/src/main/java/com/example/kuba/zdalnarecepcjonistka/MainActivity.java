package com.example.kuba.zdalnarecepcjonistka;

import android.Manifest;
import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;



public class MainActivity extends AppCompatActivity {

    private boolean doubleBackToExitPressedOnce;    // dla zmiennej typu bool wartoscia domyslna jest false

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        isStoragePermissionGranted();
    }

    /**
     * metoda sprawdza czy przyznano pozwolenie na
     * zapisywanie danych na zewnetrznej pamieci urzadzenia
     * permission is automatically granted on sdk<23 upon installation
     */
    private void isStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
            }
        }
    }

    /**
     * metoda wywoluje zapytanie o przyznanie pozwolenia na zapisywanie danych na
     * zewnetrznej pamieci urzadzenia
     * param requestCode
     * param permissions
     * param grantResults
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if(grantResults[0] != PackageManager.PERMISSION_GRANTED) {   // permission denied
            Intent intent = new Intent(MainActivity.this, MainActivity.class);
            startActivity(intent);
            finish();
        }
    }

    public void clickMain(View view) {
        BluetoothAdapter mBluetoothAdapter;
        switch (view.getId()){
            case R.id.buttonRejestracja:
                Intent intentRegistration = new Intent(MainActivity.this, RegistrationActivity.class);
                startActivity(intentRegistration);
                break;

            case R.id.buttonLogowanie:
                Intent intentSignUp = new Intent(MainActivity.this, SignInActivity.class);
                startActivity(intentSignUp);
                break;

            case R.id.buttonPobierzKlucz:
                mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
                // Check for Bluetooth support and then check to make sure it is turned on
                // Emulator doesn't support Bluetooth and will return null
                if(mBluetoothAdapter == null) {
                    Toast.makeText(MainActivity.this, "Urządzenie nie wspiera Bluetooth", Toast.LENGTH_LONG).show();
                }
                else {
                    if (mBluetoothAdapter.isEnabled()) {
                        Intent intentReceiveKey = new Intent(MainActivity.this, ReceiveKeyActivity.class);
                        startActivity(intentReceiveKey);
                    }
                    else {
                        //Prompt user to turn on Bluetooth
                        Toast.makeText(MainActivity.this, "Włącz Bluetooth, aby przejść dalej", Toast.LENGTH_LONG).show();
                        Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                        startActivityForResult(enableBtIntent, 1);
                    }
                }
                break;

            case R.id.buttonOddajKlucz:
                mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
                // Check for Bluetooth support and then check to make sure it is turned on
                // Emulator doesn't support Bluetooth and will return null
                if(mBluetoothAdapter == null) {
                    Toast.makeText(MainActivity.this, "Urządzenie nie wspiera Bluetooth", Toast.LENGTH_LONG).show();
                }
                else {
                    if (mBluetoothAdapter.isEnabled()) {
                            Intent intentReturnKey = new Intent(MainActivity.this, ReturnKeyActivity.class);
                            startActivity(intentReturnKey);
                    }
                    else {
                        //Prompt user to turn on Bluetooth
                        Toast.makeText(MainActivity.this, "Włącz Bluetooth, aby przejść dalej", Toast.LENGTH_LONG).show();
                        Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                        startActivityForResult(enableBtIntent, 1);
                    }
                }
                break;
        }
    }

    /**
     * metoda sluzy do wyjscia z aplikacji po dwukrotnym klknieciu przycisku POWROT
     */
    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Kliknij dwukrotnie przycisk powrotu, aby wyjść z aplikacji", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 2000);
    }
}